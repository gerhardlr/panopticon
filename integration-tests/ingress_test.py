from assertpy import assert_that
import logging
import requests
import os
import pytest
from collections import namedtuple
from kubernetes import client,config
import requests
import socket

@pytest.fixture()
def k8s_client():
    config.load_kube_config()
    return client


@pytest.fixture()
def local_env():
    fixture = {}
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 1))
        IP = s.getsockname()[0]
    except:
        IP = '127.0.0.1'
    finally:
        s.close()
    fixture['ip'] = IP
    return fixture

@pytest.fixture()
def elastic_settings(k8s_client):
    fixture = {}
    api = k8s_client.ExtensionsV1beta1Api()
    ingresses = api.list_namespaced_ingress('ptna')
    elastic_ingress = [ingress for ingress in ingresses.items if ingress.metadata.labels['app'] == 'elasticsearch' ][0]
    fixture['elastic_hostnames'] = [rule.host for rule in elastic_ingress.spec.rules]
    return fixture

def test_get_ip(local_env):
    logging.info(local_env['ip'])

def test_get_elastic_settings(elastic_settings):
    logging.info(type(elastic_settings['elastic_hostnames']))
    assert_that(elastic_settings['elastic_hostnames']).is_type_of(list)

def test_ingress(local_env):
    ip = local_env['ip']
    url = 'http://{}'.format(ip)
    try:
        result = requests.get(url)
    except:
       result = 'fail'
    if (result == 'fail'):
        pytest.fail("unable to get ingress on host ip {}".format(ip))

def test_elastic_host(elastic_settings,local_env):
    hosts = elastic_settings['elastic_hostnames']
    ip = ip = local_env['ip']
    for host in hosts:
        url = 'http://{}'.format(ip)
        try:
            result = requests.get(url,headers ={'host':host})
        except:
            result = 'fail'
        if (result == 'fail'):
            pytest.fail("unable to get ingress on host {}".format(host)) 
        assert_that(result.ok).is_true()