#!/usr/bin/python3

from elasticsearch_dsl import connections, Search
from datetime import datetime
from elasticsearch import Elasticsearch
from assertpy import assert_that
import logging
import pprint
import requests
import yaml
import pytest
from kubernetes import client,config

mappings = {'properties': {'author': {'type': 'text',
   'fields': {'keyword': {'type': 'keyword', 'ignore_above': 256}}},
  'text': {'type': 'text',
   'fields': {'keyword': {'type': 'keyword', 'ignore_above': 256}}}}}

logger = logging.getLogger(__name__)



#index management with es

def get_index(db,index):
    result = db.indices.get(index)
    return(result)

def delete_index(db,index):
    result = db.indices.delete(index)
    return(result)

def create_index(db,index): 
    result = db.indices.create(index)
    return(result)

def get_all_indices(db):
    return db.cat.indices(format='json')

def delete_all_indices(db):
    indices = get_all_indices(db)
    for index in indices:
        delete_index(db,index['index'])

def update_or_set_mapping(db,mapping):
    result = db.indices

#### doc management with es
def create_doc(db,index,doc):
    res = db.index(index=index, body=doc)
    return(res)

def get_doc(db,index,the_id):
    res = db.get(index=index, id=the_id)
    return(res)

def delete_doc(db,index,the_id):
    res = db.delete(index=index, id=the_id)
    return(res)

#tests fixtures
@pytest.fixture()
def k8s_api():
    config.load_kube_config()
    return client.CoreV1Api()


@pytest.fixture()
def empty_db(k8s_api):
    services = k8s_api.list_namespaced_service('ptna').items
    elasticsearch_master = [service for service in services if service.metadata.name == 'elasticsearch-master'][0]
    hostip = elasticsearch_master.spec.cluster_ip
    es = Elasticsearch([{'host':hostip}])
    yield es
    #delete any created indexes
    delete_all_indices(es)

@pytest.fixture()
def a_created_test_index(empty_db):
    fixture ={}
    fixture['transaction result'] = create_index(empty_db,'test-index')
    fixture['index name'] = 'test-index'
    fixture['db'] = empty_db
    yield fixture

@pytest.fixture()
def a_created_test_doc(a_created_test_index):
    doc = {
    'author': 'tester',
    'text': 'This is a test document, it should be deleted',
    }
    fixture = {}
    db = a_created_test_index['db']
    index_name = a_created_test_index['index name']
    fixture['index name'] = index_name
    fixture['doc'] = doc
    result = create_doc(db,index_name,doc)
    fixture['id'] = result['_id']
    fixture['transaction result'] = result
    fixture['db'] = db
    yield fixture

####tests

def test_delete_all_indices(empty_db):
    #given
    db = empty_db
    create_index(db,'test-index1')
    create_index(db,'test-index2')
    create_index(db,'test-index3')
    #when
    delete_all_indices(db)
    #then
    res = get_all_indices(db)
    assert_that(res).is_empty()

def test_create_index(empty_db):
    #given empty_db
    db = empty_db
    #when
    result = create_index(db,'test-index')
    logger.debug(result)
    #then
    assert_that(result['acknowledged']).is_equal_to(True)

def test_db(empty_db):
    assert_that(empty_db.ping()).is_true()

def test_get_index(a_created_test_index):
    #given a_created_test_index
    index = a_created_test_index['index name']
    db = a_created_test_index['db']
    #when
    result = get_index(db,index)
    logger.debug("\n"+yaml.dump(result))
    #then
    assert_that(result[index]['settings']['index']['provided_name']).is_equal_to(index)

def test_delete_index(a_created_test_index):
    #given db_with_test_index
    index = a_created_test_index['index name']
    db = a_created_test_index['db']
    #when
    result = delete_index(db,index)
    logger.debug(result)
    #then
    assert_that(result['acknowledged']).is_equal_to(True)

def test_get_indices(empty_db):
    result = get_all_indices(empty_db)
    logger.debug(result)
    assert_that(result).is_instance_of(list)

def test_get_doc(a_created_test_doc):
    #given a_created_test_doc
    id = a_created_test_doc['id']
    index = a_created_test_doc['index name']
    doc = a_created_test_doc['doc']
    db = a_created_test_doc['db']
    #when
    res = get_doc(db,index,id)
    logger.debug(res)
    #then
    assert_that(res['_source']).is_equal_to(doc)
        

def test_create_doc(a_created_test_index):
    #given
    index = a_created_test_index['index name']
    doc = {
    'author': 'tester',
    'text': 'This is a test document, it should be deleted',
    }
    db = a_created_test_index['db']
    #when
    res = create_doc(db,index,doc)
    #then
    assert_that(res['result']).is_equal_to('created')
    logger.debug("doc created successfully")
    #and
    id = res['_id']
    res = get_doc(db,index,id)
    assert_that(res['_source']).is_equal_to(doc)
    

def test_delete_doc(a_created_test_doc):
    #given
    id = a_created_test_doc['id']
    index = a_created_test_doc['index name']
    db = a_created_test_doc['db']
    #when
    logger.info("deleting the test doc")
    res = delete_doc(db,index,id)
    #then
    assert_that(res['result']).is_equal_to('deleted')




