add_hosts:
	@cat dns.txt >> /etc/hosts
	@echo "/etc/hosts updated! \n"
	@cat /etc/hosts

remove_hosts:
	grep -Fvx -f dns.txt /etc/hosts >temp && mv temp /etc/hosts
	@echo "/etc/hosts updated! \n"
	@cat /etc/hosts

deploy_ingress_controller:
	@minikube addons enable ingress

delete_ingress_controller:
	@minikube addons disable ingress

generate_tls:
	@openssl req -x509 -sha256 -nodes -days 365 -newkey rsa:2048 -keyout tls.key -out tls.crt -subj "/CN=nginxsvc/O=nginxsvc"
	@kubectl create secret tls tls-secret --key tls.key --cert tls.crt

remove_tls:
	@kubectl create secret tls tls-secret --key tls.key --cert tls.crt