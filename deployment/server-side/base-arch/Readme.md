# Set up of Base architecture for development purposes

## Order of installation

Remember to activate virtual execution environment
```
. ../../../venv/bin/activate
```

### Layer 1
```
make install_minikube
make start_minikube
```
Setup base arch namespace and a basic ingress controller (NGINX for minikube)
```
make minikube_setup
sudo make -f root.mk deploy_ingress_controller
```
 ### Layer 2
 ```
 make deploy_elastic
 make deploy_id_provider
 ```
 The id provider under evaluation is key cloak
 Take note of the startup password generated in case you want to log into the id provider.
 ```
 make deploy_gate_keeper
 ```
 The gate keeper is the enrance to the server.
 for testing purposed you may want to set up a temp dns.
 ```
 make write_dns
 sudo make -f root.mk add_hosts
 ```
 you need to do the last task as sudo to update your local dns

## Order of uninstallation
### Layer 2
if dns enabled
```
sudo make -f root.mk remove_hosts
make reset_dns
```
Then
```
make delete_gate_keeper
make delete_id_provider
make delete_elastic
```
### Layer 1
remove the namespaces and ingress
```
make minikube_set_down
sudo make -f root.mk delete_ingress_controller
```
```
make delete_minikube
```



