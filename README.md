# PANOPTICON

A system for aggregating user locacation data in order to generate information and manage about COVID-19 transmission

## Getting Started

This is a mono-repo and thus contains various modules for different kinds of software components. To deploy the system you need an ubuntu kubernetes enabled machine. For development purposes this can be deployed on a single machine as minikube

### Prerequisites

##Installing the base layer for dev purposes


```
Make -C Deployment/base-arch/ install_minikube
Make -C Deployment/base-arch/ start_minikube
Make -C Deployment/base-arch/ install_helm
Make -C Deployment/base-arch/ minikube_setup
Make -C Deployment/base-arch/ deploy_ellastic
```

The system is based on elastic search as a back end for storing location data. The current dev system uses the kubernetes overlaying network to provide a IP adress. This should later be managed as an service provided hostname but for now is manualy added in the testing system.
